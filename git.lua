local base = "gitlab.com/ConRonJohnson/computercraft/-/raw/master/"
local readme = http.get( base .. "README.md" )

for file in readme.readLine do
	local f = fs.open(file, "w")
	local git = http.get( base .. file )
	f.write(git.readAll())
	f.close()
end

