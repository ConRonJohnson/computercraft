local store = "data/mined"

local minX = -100
local maxX = 100
local minY = 20
local maxY = 100

local world = {}
for row=minY, maxY do
	world[row] = {}
	for col=minX, maxX do
		world[row][col] = "unmined"
	end
end

rednet.open("back")

local function formX( x )
	return math.max( math.min(x, maxX) , minX )
end
local function formY( y )
	return math.max( math.min(y, maxY) , minY )
end

local remaining = (maxY-minY) * math.floor( (maxX-minX) / 4 )

local f = fs.open(store, "r")
if f then
	for line in f.readLine do
		local x,y = line:gmatch( "(%d+),(%d+)" )()
		x = tonumber(x)
		y = tonumber(y)

		print("Read in: "..x..", "..y)

		if y == formY(y) and x == formX(x) then
			--coords are in min/max values
			print("Marking ("..x..", "..y..") as completed")

			world[y][x] = "done"
			world[formY(y-1)][x] = "done"
			world[formY(y+1)][x] = "done"
			world[y][formX(x-1)] = "done"
			world[y][formX(x+1)] = "done"

			remaining = remaining - 1
		end
	end
else
	f = fs.open(store, "w")
end

f.close()

local function getNext()
	for row = minY, maxY do
		for col = minX, maxX do
			if world[row][col] == "unmined" then
				return row, col
			end
			col = col + 3
		end
	end
end

while remaining > 0 do
	local id, mes, dist, prot = rednet.receive()
	local pat = mes:gmatch("%S+")
	local command = pat()
	if command == "ready" then
		local nextY, nextX = getNext()
		if nextX then
			rednet.send(id, nextX..","..nextY)
			world[nextY][nextX] = "working"
		end
	else if command == "unloading" then
		for word in pat do
			print("Received: "..word)
		end
	else if command == "finished" then
		local finX,finY = pat():gmatch(" (%d+),(%d+) ")()
		world[finY][finX] = "done"
	end
end
end
end
