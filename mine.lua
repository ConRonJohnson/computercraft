turt = require ("turt")

local counter = 0
local inspected = {}
local ignored = {
	["minecraft:stone"] = true,
	["minecraft:dirt"] = true
}

function mine()
	repeat
		counter = counter + 1
		for i=1, 4 do
			local detected, block = turtle.inspect()
			if detected then
				inspected[block.name] = inspected[block.name] or 0
				inspected[block.name] = inspected[block.name] + 1
	
				if not ignored[block.name] then
					turtle.dig()
					if turtle.getItemCount(15) > 0 then
						return false
					end
				end
			end
			turtle.turnRight()
		end
		turtle.digDown()
	until not turt:down()
	
	for i=1, counter-1 do
		turt:up()
	end
	turtle.select(1)
	for i=1, 4 do
		turtle.place()
	end
	turt:up()
	turtle.placeDown()
	return true
end

rednet.open("back")

while true do
	rednet.broadcast("ready")
	id, mes, dist, prot = rednet.receive(5)
	if not id then
		break
	end
	local x, y = mes:gmatch("(%d+),(%d+)")()
	x = tonumber(x)
	y = tonumber(y)
	repeat
		for i=1, y do
			turt:forward()
		end
		if x < 0 then
			turt:left()
		else
			turt:right()
		end
		for i=1, x do
			turt:forward()
		end
		
		turtle.select(1)
		res = mine()

		turt:home()
		for i=1, 15 do
			turtle.select(i)
			turtle.dropUp()
		end
	until res
end
