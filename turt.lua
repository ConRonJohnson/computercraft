local m = {
	["refuelSlot"] = 16,
	["path"] = {}
}

function m:refuel()
	if turtle.getFuelLevel() == 0 then
		local prev = turtle.getSelectedSlot()
		turtle.select(self.refuelSlot)
		turtle.refuel()
		turtle.select(prev)
	end
end

function m:home()
	for _, v in pairs(self.path) do
		self:refuel()
		v()
	end
	self.path = {}
end

function m:forward()
	turtle.dig()
	self:refuel()
	if turtle.forward() then
		if self.path[1] == turtle.forward then
			table.remove(self.path, 1)
		else
			table.insert(self.path, 1, turtle.back)
		end
		return true
	end
	return false
end
function m:up()
	self:refuel()
	if turtle.up() then
		if self.path[1] == turtle.up then
			table.remove(self.path, 1)
		else
			table.insert(self.path, 1, turtle.down)
		end
		return true
	end
	return false
end
function m:back()
	self:refuel()
	if turtle.back() then
		if self.path[1] == turtle.back then
			table.remove(self.path, 1)
		else
			table.insert(self.path, 1, turtle.forward)
		end
		return true
	end
	return false
end
function m:down()
	self:refuel()
	if turtle.down() then
		if self.path[1] == turtle.down then
			table.remove(self.path, 1)
		else
			table.insert(self.path, 1, turtle.up)
		end
		return true
	end
	return false
end
function m:left()
	if self.path[1] == turtle.turnLeft then
		table.remove(self.path, 1)
	else
		table.insert(self.path, 1, turtle.turnRight)
	end
	turtle.turnLeft()
end
function m:right()
	if self.path[1] == turtle.turnRight then
		table.remove(self.path, 1)
	else
		table.insert(self.path, 1, turtle.turnLeft)
	end
	turtle.turnRight()
end


return m
